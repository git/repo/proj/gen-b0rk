# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Ensure ROOT variable usage complies with the PMS"
HOMEPAGE="https://dev.gentoo.org/~ulm/pms/head/pms.html#x1-11800011"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# All uses of $ROOT within the pkg_* "Ebuild-defined functions" should
# be allowed. The list comes from the so-named chapter of the PMS.

pkg_pretend() {
	local foo=$ROOT
}

pkg_setup() {
	local foo="${ROOT}/foo"
}

pkg_preinst() {
	local foo="$ROOT"
}

pkg_postinst() {
	local foo="bar/${ROOT}"
}

pkg_prerm() {
	local foo="bar/${ROOT}/baz"
}

pkg_postrm() {
	local foo=bar/$ROOT
}

pkg_config() {
		local foo="$ROOT"
}

pkg_info() {
local foo=$ROOT
}

pkg_nofetch() {
	local foo=`echo $ROOT`
}

# All uses below here are errors. The following src_* functions are
# defined in the PMS.
src_unpack() {
	local foo=$ROOT
}

src_prepare() {
	local foo="${ROOT}/foo"
}

src_configure() {
	local foo=`echo $ROOT`
}

src_compile() {
local foo=$ROOT
}

src_test() {
		local foo=$(echo $ROOT)
}

src_install() {
	local foo="bar/${ROOT}/baz"
}

pkg_apocrypha(){
	# This function begins with "pkg_", but isn't defined in the PMS.
	local foo=bar/$ROOT
}

washington_irving(){
	# This function is arbitrarily-named and not defined in the PMS.
	local foo="${ROOT}/foo"
}

# And I suppose we should check that it's not used in global scope, too.
DEPEND="sys-libs${ROOT}glibc"
