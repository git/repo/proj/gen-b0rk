# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Pass D/ROOT/EPREFIX to helpers"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_install() {
	into "${D}/usr"
	insinto "${ED}/usr/lib"
	docinto "${EROOT}/usr/share/doc"
}
