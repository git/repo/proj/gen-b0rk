# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Stale $Id$ line in header"
HOMEPAGE="http://example.com/"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64"
