# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Minor syntax errors"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

WANT_AUTOMAKE=latest

src_unpack() {
	epatch
}

src_compile() {
	econf
}

src_install() {
	cd "${S}"
	dodoc COPYING
}
