# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Minor syntax errors: deprecated functions"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_configure() {
	built_with_use foo
	useq foo
	hasq foo
	: $(bindnow-flags)
	preserve_old_lib foo
}
