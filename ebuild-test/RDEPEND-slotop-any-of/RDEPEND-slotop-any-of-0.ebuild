# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Pass ':=' to '||' conditionals"
HOMEPAGE="http://gentoo.org/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
# Correct dependency for this case would be to move ':=' out of 'any-of' clause:
# RDEPEND="
#	not-broken/pkg1-subslot:=
#	|| ( =not-broken/pkg1-subslot-0 =not-broken/pkg1-subslot-1 )"
RDEPEND="|| ( =not-broken/pkg1-subslot-0:= =not-broken/pkg1-subslot-1:0= )"
