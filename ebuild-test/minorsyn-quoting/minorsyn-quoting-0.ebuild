# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Minor syntax errors: variable quoting"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_unpack() {
	# A should never be quoted when passed to unpack
	unpack "${A}"
	# DISTDIR, WORKDIR, S should always be quoted
	cp ${DISTDIR}/foo ${WORKDIR}/${P}
	cd ${S}
}

src_prepare() {
	# unquoted FILESDIR
	eapply ${FILESDIR}/foo.patch
}

src_install() {
	# unquoted ED
	install foo ${ED}
}

pkg_postinst() {
	# Variables do not need to be quoted in bash conditionals or for einfo.
	# Variables do need to be quoted when passed to a real command.
	if [[ ! -e ${EROOT}/foo ]]; then
		touch ${EROOT}/foo
		einfo installed foo to ${EROOT}
	fi
}
