# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Various workarounds for broken code upstream"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_compile() {
	addpredict /foo
	emake -j1
	MAKEOPTS="-j1" emake
	emake LDFLAGS="$(no-as-needed)"
}
