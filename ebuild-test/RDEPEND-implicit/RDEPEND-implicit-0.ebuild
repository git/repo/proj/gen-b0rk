# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=3

DESCRIPTION="Check for implicit RDRPEND=DEPEND for old EAPIs"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="sys-libs/zlib:0"
