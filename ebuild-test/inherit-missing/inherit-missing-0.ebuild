# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Missing inherits"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

src_prepare() {
	# missing autotools
	eautoreconf
}

src_configure() {
	# missing eutils
	edos2unix foo
	# missing flag-o-matic
	strip-flags
}
