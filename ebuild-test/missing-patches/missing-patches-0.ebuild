# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

DESCRIPTION="Missing patches"
HOMEPAGE="https://foo.example.org/"
SRC_URI="https://dev.gentoo.org/~jmbsvicetto/distfiles/${P}.tar.bz2"
LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
DEPEND=""
RDEPEND="${DEPEND}"
PATCHES=(
	"${FILESDIR}/files-patch-1.patch"
	"${FILESDIR}/files-patch-2.patch"
	"${S}/patch-1.patch"
	"${S}/patch-2.patch"
)
