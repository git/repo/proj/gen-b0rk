# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=0

inherit base

DESCRIPTION="Inherit deprecated eclass"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
