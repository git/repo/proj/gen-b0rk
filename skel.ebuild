# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Minimal ebuild for creating tests"
HOMEPAGE="http://example.com/"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
